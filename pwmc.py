#!/usr/bin/env python

from glass.HTTPServer import *


try:
	print "Use Control-C to exit.  In Windows, use Control-Break."
	server_address = ('', 8000)
	httpd = HTTPServer (server_address, None, None, None)
	httpd.RequestHandlerClass.protocol_version = "HTTP/1.0"
	sa = httpd.socket.getsockname ()
	print "Serving HTTP on", sa[0], "port", sa[1], "..."
	httpd.serve_forever ()

except KeyboardInterrupt:
	pass
